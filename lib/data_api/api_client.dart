import 'dart:async';

import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '../core/entities/pageable/pageable.dart';
import '../core/entities/token/token.dart';
import '../core/entities/transaction/transaction.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: 'https://syntaxis-api-manual.staging.syntaxis.nl/api')
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @POST('/auth/authenticate/transaction-only')
  Future<Token> authenticateStreeplijst(@Body() LoginRequest body);

  @GET('/products')
  Future<Pageable> getProducts(@Query('category') int? categoryId,
      {@Query('size') int size = 1000});

  /*@GET('/products')
  Future<List<Product>> getProductsById(@Query('category') int categoryId);*/

  @GET('/discounts')
  Future<Pageable> getDiscounts({@Query('size') int size = 1000});

  @GET('/categories')
  Future<Pageable> getCategories({@Query('size') int size = 1000});

  @POST('/transactions')
  Future<TransactionRead> createTransaction(
    @Body() TransactionWrite transaction,
  );
}
