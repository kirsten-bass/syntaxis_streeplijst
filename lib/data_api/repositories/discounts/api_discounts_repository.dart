import '../../../core/entities/discount/discount.dart';
import '../../../core/repositories/api_repository/api_repository.dart';
import '../../../core/repositories/discounts/discounts_repository.dart';
import '../../api_client.dart';

class ApiDiscountsRepository extends ApiRepository with DiscountsRepository {
  ApiDiscountsRepository(ApiClient client) : super(client);

  @override
  Future<List<Discount>> getDiscounts() async {
    final pageable = await client.getDiscounts();

    return pageable.content.map(Discount.fromJson).toList();
  }
}
