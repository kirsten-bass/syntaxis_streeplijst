import 'package:dio/dio.dart';

import '../../../core/entities/token/token.dart';
import '../../../core/repositories/login/login_repository.dart';
import '../../api_client.dart';

class ApiLoginRepository implements LoginRepository {
  @override
  Future<Token?> login(String barcode) {
    final apiClient = ApiClient(Dio());

    return apiClient.authenticateStreeplijst(LoginRequest(token: barcode));
  }
}
