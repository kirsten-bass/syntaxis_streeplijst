import '../../../core/entities/user/user.dart';
import '../../../core/repositories/api_repository/api_repository.dart';
import '../../../core/repositories/user/user_repository.dart';
import '../../api_client.dart';


class ApiUserRepository extends ApiRepository with UserRepository {
  ApiUserRepository(ApiClient client) : super(client);


  @override
  Future<double> getUserBalance(int userId) async {
    return -1000.0;
  }

}
