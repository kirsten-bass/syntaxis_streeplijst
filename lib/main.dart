import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/providers/authentication/authentication_provider.dart';
import 'core/providers/provider_builder.dart';
import 'core/use_cases/start/app_started_use_case.dart';
import 'ui/start/nav/cubit/login_cubit.dart';
import 'ui/start/nav/streeplijst_router.dart';
import 'ui/start/nav/streeplijst_start.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return EasyLocalization(
      startLocale: const Locale('nl'),
      supportedLocales: const [
        Locale('nl'),
        Locale('en'),
      ],
      path: 'assets/lang',
      fallbackLocale: const Locale('en'),
      child: MultiRepositoryProvider(
        key: const ValueKey('app'),
        providers: ProviderBuilder.buildLoginMock(),
        child: _SyntaxisStreeplijst(title: 'Syntaxis Streeplijst'),
      ),
    );
  }
}

class _SyntaxisStreeplijst extends StatelessWidget {
  _SyntaxisStreeplijst({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        final authProvider = context.read<AuthenticationProvider>();
        final appStarted = AppStartedUseCase(authProvider);
        return LoginCubit(appStarted);
      },
      lazy: false,
      child: Builder(
        builder: (context) {
          final cubit = context.read<LoginCubit>();
          return RepositoryProvider(
            create: (context) => StreeplijstRouter(cubit),
            child: StreeplijstStart(),
          );
        },
      ),
    );
  }
}
