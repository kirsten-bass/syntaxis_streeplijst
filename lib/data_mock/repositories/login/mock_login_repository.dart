import '../../../core/entities/token/token.dart';
import '../../../core/entities/user/user.dart';
import '../../../core/repositories/login/login_repository.dart';

class MockLoginRepository implements LoginRepository {
  @override
  Future<Token?> login(String barcode) async {
    return Token(
      accessToken: '',
      refreshToken: '',
      user: User(
        firstname: 'Sjoemel',
        lastname: 'Kuster',
        id: 1733,
        //discount: true,
        //roles: 'ADMIN',
        //email: 'daniel.roek@gmail.com',
        //barcode: 'ASDJESNCP',
      ),
    );
  }
}
