import '../../../core/entities/transaction/transaction.dart';
import '../../../core/repositories/transaction/transaction_repository.dart';

class MockTransactionsRepository implements TransactionsRepository {
  @override
  Future<void> addTransaction(TransactionWrite transaction) {
    return Future.delayed(Duration(milliseconds: 200));
  }
}
