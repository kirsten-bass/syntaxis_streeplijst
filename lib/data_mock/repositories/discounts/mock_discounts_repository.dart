import '../../../core/entities/discount/discount.dart';
import '../../../core/repositories/discounts/discounts_repository.dart';

class MockDiscountsRepository implements DiscountsRepository {
  @override
  Future<List<Discount>> getDiscounts() async {
    return [
      Discount(name: 'Actieve leden korting', percentage: 0.15, id: 1),
    ];
  }
}
