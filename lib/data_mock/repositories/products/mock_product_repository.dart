import '../../../core/entities/image/image.dart';
import '../../../core/entities/product/product.dart';
import '../../../core/repositories/products/products_repository.dart';

class MockProductsRepository implements ProductsRepository {
  @override
  Future<Product> getProductById(int id) async {
    // TODO: implement getProductById
    throw UnimplementedError();
  }

  @override
  Future<List<Product>> getProducts() async {
    return products;
  }

  @override
  Future<List<Product>> getProductByCategoryId(int id) async {
    await Future.delayed(Duration(milliseconds: 200));
    return products;
  }

  final products = const <Product>[
    Product(
      id: 123,
      price: 1.00,
      name: 'Zero sugar',
      deposit: 0,
      discountPct: 0.0,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 124,
      price: 0.60,
      name: 'Orange',
      deposit: 0.15,
      discountPct: 0.0,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 125,
      price: 1.79,
      name: 'Rood fruit',
      deposit: 0.25,
      discountPct: 0.15,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 126,
      price: 1.23,
      name: '4 uur',
      deposit: 0,
      discountPct: 0.15,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 127,
      price: 1.22,
      name: 'Premium Pilsner',
      deposit: 0,
      discountPct: 0.15,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 128,
      price: 1.22,
      name: 'Premium Pilsner',
      deposit: 0,
      discountPct: 0.15,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 129,
      price: 1.22,
      name: 'Premium Pilsner',
      deposit: 0,
      discountPct: 0.15,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 130,
      price: 1.22,
      name: 'Premium Pilsner',
      deposit: 0,
      discountPct: 0.15,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
    Product(
      id: 131,
      price: 12220,
      name: 'Premium Pilsner',
      deposit: 0,
      discountPct: 0.15,
      image: Image(
        id: 123,
        extension: '.jpg',
        name: 'image',
      ),
    ),
  ];
}
