// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'shopping_basket_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ShoppingBasketState {
  List<ShoppingBasketItem> get shoppingBasketItems =>
      throw _privateConstructorUsedError;
  double get totalPrice => throw _privateConstructorUsedError;
  double get totalDiscount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ShoppingBasketStateCopyWith<ShoppingBasketState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShoppingBasketStateCopyWith<$Res> {
  factory $ShoppingBasketStateCopyWith(
          ShoppingBasketState value, $Res Function(ShoppingBasketState) then) =
      _$ShoppingBasketStateCopyWithImpl<$Res, ShoppingBasketState>;
  @useResult
  $Res call(
      {List<ShoppingBasketItem> shoppingBasketItems,
      double totalPrice,
      double totalDiscount});
}

/// @nodoc
class _$ShoppingBasketStateCopyWithImpl<$Res, $Val extends ShoppingBasketState>
    implements $ShoppingBasketStateCopyWith<$Res> {
  _$ShoppingBasketStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shoppingBasketItems = null,
    Object? totalPrice = null,
    Object? totalDiscount = null,
  }) {
    return _then(_value.copyWith(
      shoppingBasketItems: null == shoppingBasketItems
          ? _value.shoppingBasketItems
          : shoppingBasketItems // ignore: cast_nullable_to_non_nullable
              as List<ShoppingBasketItem>,
      totalPrice: null == totalPrice
          ? _value.totalPrice
          : totalPrice // ignore: cast_nullable_to_non_nullable
              as double,
      totalDiscount: null == totalDiscount
          ? _value.totalDiscount
          : totalDiscount // ignore: cast_nullable_to_non_nullable
              as double,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ShoppingBasketStateCopyWith<$Res>
    implements $ShoppingBasketStateCopyWith<$Res> {
  factory _$$_ShoppingBasketStateCopyWith(_$_ShoppingBasketState value,
          $Res Function(_$_ShoppingBasketState) then) =
      __$$_ShoppingBasketStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<ShoppingBasketItem> shoppingBasketItems,
      double totalPrice,
      double totalDiscount});
}

/// @nodoc
class __$$_ShoppingBasketStateCopyWithImpl<$Res>
    extends _$ShoppingBasketStateCopyWithImpl<$Res, _$_ShoppingBasketState>
    implements _$$_ShoppingBasketStateCopyWith<$Res> {
  __$$_ShoppingBasketStateCopyWithImpl(_$_ShoppingBasketState _value,
      $Res Function(_$_ShoppingBasketState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shoppingBasketItems = null,
    Object? totalPrice = null,
    Object? totalDiscount = null,
  }) {
    return _then(_$_ShoppingBasketState(
      shoppingBasketItems: null == shoppingBasketItems
          ? _value.shoppingBasketItems
          : shoppingBasketItems // ignore: cast_nullable_to_non_nullable
              as List<ShoppingBasketItem>,
      totalPrice: null == totalPrice
          ? _value.totalPrice
          : totalPrice // ignore: cast_nullable_to_non_nullable
              as double,
      totalDiscount: null == totalDiscount
          ? _value.totalDiscount
          : totalDiscount // ignore: cast_nullable_to_non_nullable
              as double,
    ));
  }
}

/// @nodoc

class _$_ShoppingBasketState implements _ShoppingBasketState {
  const _$_ShoppingBasketState(
      {required this.shoppingBasketItems,
      required this.totalPrice,
      required this.totalDiscount});

  @override
  final List<ShoppingBasketItem> shoppingBasketItems;
  @override
  final double totalPrice;
  @override
  final double totalDiscount;

  @override
  String toString() {
    return 'ShoppingBasketState(shoppingBasketItems: $shoppingBasketItems, totalPrice: $totalPrice, totalDiscount: $totalDiscount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ShoppingBasketState &&
            const DeepCollectionEquality()
                .equals(other.shoppingBasketItems, shoppingBasketItems) &&
            (identical(other.totalPrice, totalPrice) ||
                other.totalPrice == totalPrice) &&
            (identical(other.totalDiscount, totalDiscount) ||
                other.totalDiscount == totalDiscount));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(shoppingBasketItems),
      totalPrice,
      totalDiscount);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ShoppingBasketStateCopyWith<_$_ShoppingBasketState> get copyWith =>
      __$$_ShoppingBasketStateCopyWithImpl<_$_ShoppingBasketState>(
          this, _$identity);
}

abstract class _ShoppingBasketState implements ShoppingBasketState {
  const factory _ShoppingBasketState(
      {required final List<ShoppingBasketItem> shoppingBasketItems,
      required final double totalPrice,
      required final double totalDiscount}) = _$_ShoppingBasketState;

  @override
  List<ShoppingBasketItem> get shoppingBasketItems;
  @override
  double get totalPrice;
  @override
  double get totalDiscount;
  @override
  @JsonKey(ignore: true)
  _$$_ShoppingBasketStateCopyWith<_$_ShoppingBasketState> get copyWith =>
      throw _privateConstructorUsedError;
}
