import 'package:bloc/bloc.dart';

import '../../../../../core/entities/discount/discount.dart';
import '../../../../../core/entities/product/product.dart';
import '../../../../../core/entities/transaction/transaction.dart';
import '../../../../../core/providers/authentication/authentication_provider.dart';
import '../../../../../core/use_cases/checkout/create_transaction_use_case.dart';
import 'shopping_basket_state.dart';

class ShoppingBasketCubit extends Cubit<ShoppingBasketState> {
  final CreateTransactionUseCase _createTransactionUseCase;
  final AuthenticationProvider _authenticationProvider;
  final List<Discount> _discounts;

  ShoppingBasketCubit(
    this._createTransactionUseCase,
    this._authenticationProvider,
    this._discounts,
  ) : super(
          ShoppingBasketState(
            shoppingBasketItems: [],
            totalDiscount: 0,
            totalPrice: 0,
          ),
        );

  int? getIndexOfProduct(Product product) {
    final items = state.shoppingBasketItems;
    for (var shoppingBasketItem in items) {
      if (shoppingBasketItem.product.id == product.id) {
        return items.indexOf(shoppingBasketItem);
      }
    }
    return null;
  }

  int? getIndexOfProductById(int id) {
    final items = state.shoppingBasketItems;
    for (var shoppingBasketItem in items) {
      if (shoppingBasketItem.product.id == id) {
        return items.indexOf(shoppingBasketItem);
      }
    }
    return null;
  }

  void addProduct(Product product, int amount) {
    final items = state.shoppingBasketItems;
    items.add(ShoppingBasketItem(product, amount));

    emit(
      state.copyWith(
          shoppingBasketItems: items,
          totalDiscount: _calculateTotalDiscount(items),
          totalPrice: _calculateTotalPrice(items)),
    );
  }

  void addOneToProduct(int productId) {
    var index = getIndexOfProductById(productId);

    if (index == null) {
      return;
    }

    final items = List<ShoppingBasketItem>.from(state.shoppingBasketItems);

    items[index] = items[index].increaseAmount();

    emit(
      state.copyWith(
          shoppingBasketItems: items,
          totalDiscount: _calculateTotalDiscount(items),
          totalPrice: _calculateTotalPrice(items)),
    );
  }

  void removeOneFromProduct(int productId) {
    var index = getIndexOfProductById(productId);

    if (index == null) {
      return;
    }

    final items = List<ShoppingBasketItem>.from(state.shoppingBasketItems);

    items[index] = items[index].decreaseAmount();

    if (items[index].amount <= 0) {
      items.removeAt(index);
    }

    emit(
      state.copyWith(
          shoppingBasketItems: items,
          totalDiscount: _calculateTotalDiscount(items),
          totalPrice: _calculateTotalPrice(items)),
    );
  }

  void removeProduct(int index) {
    var items = List<ShoppingBasketItem>.from(state.shoppingBasketItems);
    items.removeAt(index);
    emit(
      state.copyWith(
          shoppingBasketItems: items,
          totalDiscount: _calculateTotalDiscount(items),
          totalPrice: _calculateTotalPrice(items)),
    );
  }

  Future<void> checkout() async {
    await _createTransactionUseCase(
      TransactionWrite(
        transactionItems: state.toTransactionItems(),
      ),
    ).then((value) => _authenticationProvider.logout());
  }

  double _calculateTotalPrice(List<ShoppingBasketItem> items) {
    if (_discounts.isNotEmpty) {
      return items.fold(
          0,
          (total, item) =>
              total +
              (item.product.price *
                  (1 - _discounts[0].percentage) *
                  item.amount) +
              item.amount * item.product.deposit);
    } else {
      return items.fold(
          0,
          (total, item) =>
              total +
              ((item.product.price + item.product.deposit) * item.amount));
    }
  }

  double _calculateTotalDiscount(List<ShoppingBasketItem> items) {
    if (_discounts.isNotEmpty) {
      return items.fold(
        0,
        (total, item) =>
            total +
            (item.product.price * _discounts[0].percentage) * item.amount,
      );
    }
    return 0;
  }
}
