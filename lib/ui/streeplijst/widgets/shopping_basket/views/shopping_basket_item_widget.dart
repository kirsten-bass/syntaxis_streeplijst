import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/entities/product/product.dart';
import '../../../../../utils/extensions.dart';
import '../../shopping_basket/cubit/shopping_basket_cubit.dart';
import '../../shopping_basket/cubit/shopping_basket_state.dart';

class ShoppingBasketItemWidget extends StatelessWidget {
  final Product product;
  final int amount;

  const ShoppingBasketItemWidget({
    Key? key,
    required this.product,
    required this.amount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 88,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 12),
            child: SizedBox(
              width: 28,
              child: Text(
                amount.toString(),
                style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                      color: (Theme.of(context).colorScheme.secondary
                              as MaterialColor)
                          .shade600,
                    ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16, left: 8),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              // Item name and price
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  product.name,
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                Row(
                  children: [
                    Text(
                      '${(product.totalPrice * amount).toCurrency()} ',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            color: (Theme.of(context).colorScheme.secondary
                                    as MaterialColor)
                                .shade600,
                          ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, bottom: 4),
                  child: _IconButton(
                    icon: Icons.add,
                    onTap: () {
                      context
                          .read<ShoppingBasketCubit>()
                          .addOneToProduct(product.id);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, top: 4),
                  child: _IconButton(
                    icon: Icons.remove,
                    onTap: () {
                      context
                          .read<ShoppingBasketCubit>()
                          .removeOneFromProduct(product.id);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _IconButton extends StatelessWidget {
  final VoidCallback? onTap;
  final IconData icon;

  const _IconButton({required this.onTap, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(4),
      color: onTap == null
          ? (Theme.of(context).colorScheme.primary as MaterialColor)[400]
          : Theme.of(context).colorScheme.secondary,
      child: DecoratedBox(
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(4),
        ),
        child: SizedBox.square(
          dimension: 40,
          child: InkWell(
            borderRadius: BorderRadius.circular(4),
            child: Icon(
              icon,
              color: Theme.of(context).colorScheme.onSecondary,
            ),
            onTap: onTap,
          ),
        ),
      ),
    );
  }
}
