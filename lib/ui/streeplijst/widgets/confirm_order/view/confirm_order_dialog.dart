import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../utils/extensions.dart';
import '../../../../shared/call_to_action_button.dart';
import '../../shopping_basket/cubit/shopping_basket_cubit.dart';
import '../../shopping_basket/cubit/shopping_basket_state.dart';
import '../widget/confirm_order_dialog_row.dart';

class ConfirmOrderDialog extends StatelessWidget {
  final _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ShoppingBasketCubit, ShoppingBasketState>(
      builder: (context, state) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'confirm_order'.tr(),
                  style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ),
            ),
            Divider(
              color: (Theme.of(context).colorScheme.primary as MaterialColor)
                  .shade200,
              thickness: 1,
            ),
            Flexible(
              child: Scrollbar(
                controller: _controller,
                trackVisibility: true,
                thumbVisibility: true,
                child: ListView.separated(
                  controller: _controller,
                  itemCount: state.shoppingBasketItems.length,
                  shrinkWrap: true,
                  itemBuilder: (_, index) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: ConfirmOrderDialogRow(
                      product: state.shoppingBasketItems[index].product,
                      amount: state.shoppingBasketItems[index].amount,
                    ),
                  ),
                  separatorBuilder: (_, index) => Divider(
                    color:
                        (Theme.of(context).colorScheme.primary as MaterialColor)
                            .shade200,
                    height: 16,
                    thickness: 1,
                    indent: 8,
                    endIndent: 8,
                  ),
                ),
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (state.shoppingBasketItems.isNotEmpty)
                  Divider(
                    color:
                        (Theme.of(context).colorScheme.primary as MaterialColor)
                            .shade200,
                    thickness: 1,
                  ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 32, vertical: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('subtotal'.tr()),
                      Text(
                          (state.totalPrice + state.totalDiscount).toCurrency())
                    ],
                  ),
                ),
                Divider(
                  color:
                      (Theme.of(context).colorScheme.primary as MaterialColor)
                          .shade200,
                  thickness: 1,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 32, vertical: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('discount'.tr()),
                      Text('- ${state.totalDiscount.toCurrency()}')
                    ],
                  ),
                ),
                Divider(
                  color:
                      (Theme.of(context).colorScheme.primary as MaterialColor)
                          .shade200,
                  thickness: 1,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 32, vertical: 8),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'total'.tr(),
                        style:
                            Theme.of(context).textTheme.headlineSmall!.copyWith(
                                  fontWeight: FontWeight.bold,
                                ),
                      ),
                      Text(state.totalPrice.toCurrency())
                    ],
                  ),
                ),
                Divider(
                  color:
                      (Theme.of(context).colorScheme.primary as MaterialColor)
                          .shade200,
                  thickness: 1,
                ),
                Padding(
                  padding: EdgeInsets.all(32),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      OutlinedButton(
                        onPressed: () => Navigator.of(context).pop(false),
                        style: OutlinedButton.styleFrom(
                          side: BorderSide(
                            color: Color(0xFFF17105),
                          ),
                        ),
                        child: Text(
                          'cancel'.tr(),
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall!
                              .copyWith(
                                color: Colors.black,
                              ),
                        ),
                      ),
                      CallToActionButton(
                        text: 'checkout'.tr(),
                        onPressed: (state.shoppingBasketItems.isNotEmpty)
                            ? () => Navigator.of(context).pop(true)
                            : null,
                        backgroundColor:
                            Theme.of(context).colorScheme.secondary,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        );
      },
    );
  }
}
