import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/entities/product/product.dart';
import '../../../core/providers/authentication/authentication_provider.dart';
import '../../../utils/extensions.dart';
import '../cubit/streeplijst_cubit.dart';
import 'add_to_card_widget.dart';

class ProductCard extends StatelessWidget {
  final Product product;
  final VoidCallback onTap;

  const ProductCard({
    Key? key,
    required this.product,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final discounts = context.read<StreeplijstCubit>().state.discounts!;

    return Stack(
      clipBehavior: Clip.none,
      children: [
        Card(
          clipBehavior: Clip.antiAlias,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          child: InkWell(
            onTap: onTap,
            child: Column(
              children: [
                Container(
                  height: 240,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: CachedNetworkImageProvider(
                        'http://placekitten.com/240/240',
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Divider(
                  height: 10,
                  thickness: 1,
                  color: (theme.colorScheme.primary as MaterialColor).shade200,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 8, bottom: 16, right: 8),
                  child: Column(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          product.name,
                          style: theme.textTheme.bodyMedium,
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          product.hasDeposit
                              ? Text(
                                  'deposit'
                                      .tr(args: [product.deposit.toCurrency()]),
                                  style: theme.textTheme.titleSmall,
                                )
                              : Expanded(child: const SizedBox.shrink()),
                          Text(
                            '${(product.price + product.deposit).toCurrency()}',
                            style: Theme.of(context).textTheme.bodyMedium,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Divider(
                  height: 1,
                  color: (theme.colorScheme.primary as MaterialColor).shade200,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: AddToCardWidget(product: product),
                ),
              ],
            ),
          ),
        ),
        if (product.hasDiscount && discounts.isNotEmpty)
          Positioned(
            top: -4,
            left: -6,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
              decoration: const BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomRight: Radius.circular(8),
                ),
              ),
              child: Text(
                '- %${(discounts[0].percentage * 100).toInt()}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
      ],
    );
  }
}
