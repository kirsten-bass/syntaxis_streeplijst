import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../core/entities/category/category.dart';
import '../../shared/call_to_action_button.dart';
import '../cubit/streeplijst_cubit.dart';

class CategoryDrawer extends StatelessWidget {
  const CategoryDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black,
      child: BlocBuilder<StreeplijstCubit, StreeplijstState>(
        builder: (context, state) {
          if (state.isCategoriesLoaded) {
            return Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(16),
                  child: Image.asset(
                    'assets/images/logo_with_squiggles.png',
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16, right: 16, bottom: 48),
                  child: Center(
                    child: Text(
                      'my_streeplijst'.tr(),
                      style: TextStyle(
                        height: 1.5,
                        fontFamily: 'checkbook',
                        fontSize: 34,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                ..._buildItems(context, state.categories!, state.categoryIndex),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: IntrinsicWidth(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8),
                            child: OutlinedButton(
                              child: Text(
                                'logout'.tr(),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                      color: Colors.white,
                                    ),
                              ),
                              onPressed:
                                  context.read<StreeplijstCubit>().logout,
                              style: OutlinedButton.styleFrom(
                                fixedSize: null,
                                maximumSize: null,
                                minimumSize: Size(200, 70),
                                side: BorderSide(
                                  color: Color(0xFFF17105),
                                  width: 2,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 32),
                            child: Text(
                              'footer_drawer'.tr(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  List<Widget> _buildItems(
      BuildContext context, List<Category> categories, int selectedIndex) {
    return List.of(() sync* {
      for (var index = 0; index < categories.length; index++) {
        final category = categories[index];
        yield _Category(
          name: category.name,
          selected: index == selectedIndex,
          onSelect: () =>
              context.read<StreeplijstCubit>().selectCategory(index),
        );
      }
    }());
  }
}

class _Category extends StatelessWidget {
  final String name;
  final bool selected;
  final VoidCallback onSelect;

  const _Category({
    Key? key,
    required this.name,
    required this.selected,
    required this.onSelect,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 72,
      color: selected
          ? Color.fromARGB(255, 119, 193, 69).withOpacity(0.6)
          : Colors.black,
      child: InkWell(
        onTap: onSelect,
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Text(
              name,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
