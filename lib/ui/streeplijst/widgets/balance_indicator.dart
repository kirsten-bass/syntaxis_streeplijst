import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/extensions.dart';
import '../cubit/streeplijst_cubit.dart';

class BalanceIndicator extends StatelessWidget {
  const BalanceIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StreeplijstCubit, StreeplijstState>(
      builder: (context, state) {
        if (state.isBalanceLoaded) {
          return Container(
            child: Text('balance'.tr(args: [state.balance!.toCurrency()]),
                style: Theme.of(context).textTheme.bodyMedium),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
