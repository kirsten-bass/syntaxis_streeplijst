import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../../../core/providers/authentication/authentication_provider.dart';
import '../../../core/providers/provider_builder.dart';
import '../../../core/repositories/categories/categories_repository.dart';
import '../../../core/repositories/discounts/discounts_repository.dart';
import '../../../core/repositories/products/products_repository.dart';
import '../../../core/repositories/transaction/transaction_repository.dart';
import '../../../core/repositories/user/user_repository.dart';
import '../../../core/use_cases/authentication/logout_use_case.dart';
import '../../../core/use_cases/balance/get_user_balance_use_case.dart';
import '../../../core/use_cases/categories/get_categories_use_case.dart';
import '../../../core/use_cases/checkout/create_transaction_use_case.dart';
import '../../../core/use_cases/discount/load_discounts_use_case.dart';
import '../../../core/use_cases/products/load_products_by_category_id_use_case.dart';
import '../../shared/call_to_action_button.dart';
import '../cubit/streeplijst_cubit.dart';
import '../widgets/balance_indicator.dart';
import '../widgets/category_drawer.dart';
import '../widgets/shopping_basket/cubit/shopping_basket_cubit.dart';
import '../widgets/shopping_basket/views/shopping_basket_widget.dart';
import 'product_overview.dart';

class StreeplijstPage extends StatelessWidget {
  static const String pagePath = '/streeplijst';

  static void route(BuildContext context) => context.go(pagePath);

  const StreeplijstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: ProviderBuilder.buildAppMock(),
      child: Builder(
        builder: (context) {
          return BlocProvider<StreeplijstCubit>(
            lazy: false,
            create: (context) => StreeplijstCubit(
              LogoutUseCase(context.read<AuthenticationProvider>()),
              GetUserBalanceUseCase(
                context.read<UserRepository>(),
                context.read<AuthenticationProvider>(),
              ),
              GetCategoriesUseCase(context.read<CategoriesRepository>()),
              LoadProductsByCategoryIdUseCase(
                  context.read<ProductsRepository>()),
              LoadDiscountsUseCase(context.read<DiscountsRepository>()),
            )..init(),
            child: Builder(
              builder: (context) =>
                  BlocBuilder<StreeplijstCubit, StreeplijstState>(
                builder: (context, state) {
                  if (!state.isInitialized) {
                    return Stack(
                      children: [
                        SizedBox.expand(
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: AssetImage(
                                    'assets/images/landingpage_background.png'),
                              ),
                            ),
                          ),
                        ),
                        Center(
                          child: CircularProgressIndicator(
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                      ],
                    );
                  } else {
                    return BlocProvider<ShoppingBasketCubit>(
                      lazy: true,
                      create: (context) => ShoppingBasketCubit(
                        CreateTransactionUseCase(
                            context.read<TransactionsRepository>()),
                        context.read<AuthenticationProvider>(),
                        context.read<StreeplijstCubit>().state.discounts ?? [],
                      ),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 288,
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: CategoryDrawer(),
                            ),
                          ),
                          Expanded(
                            child: Scaffold(
                              appBar: AppBar(
                                toolbarHeight: 94,
                                backgroundColor: Colors.white,
                                leading: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 32),
                                    child: Text(
                                        'hello'.tr(
                                          args: [
                                            context
                                                    .read<
                                                        AuthenticationProvider>()
                                                    .user!
                                                    .fullname ??
                                                'enter_name_in_app'.tr()
                                          ],
                                        ),
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyMedium),
                                  ),
                                ),
                                leadingWidth: 600,
                                actions: [
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Padding(
                                      padding:
                                          const EdgeInsets.only(right: 8.0),
                                      child: BalanceIndicator(),
                                    ),
                                  ),
                                ],
                              ),
                              body: ProductOverview(),
                            ),
                          ),
                          SizedBox(
                            width: 288,
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: ShoppingBasketWidget(),
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                },
              ),
            ),
          );
        },
      ),
    );
  }
}
