import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../cubit/streeplijst_cubit.dart';
import '../widgets/product_card.dart';
import '../widgets/shopping_basket/cubit/shopping_basket_cubit.dart';

class ProductOverview extends StatelessWidget {
  final _controller = ScrollController();

  ProductOverview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StreeplijstCubit, StreeplijstState>(
      builder: (context, state) {
        if (state.isCategoriesLoaded) {
          return Scrollbar(
            thumbVisibility: true,
            trackVisibility: true,
            controller: _controller,
            child: AlignedGridView.custom(
              controller: _controller,
              crossAxisSpacing: 32,
              mainAxisSpacing: 32,
              gridDelegate: SliverSimpleGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 400,
              ),
              itemCount: state.products?.length ?? 0,
              padding: const EdgeInsets.all(16.0),
              itemBuilder: (context, index) {
                return ProductCard(
                  product: state.products![index],
                  onTap: () {
                    final cubit = context.read<ShoppingBasketCubit>();

                    if (cubit.getIndexOfProduct(state.products![index]) != null) {
                      cubit.addOneToProduct(state.products![index].id);
                    } else {
                      cubit.addProduct(state.products![index], 1);
                    }
                  },
                );
              },
            ),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
