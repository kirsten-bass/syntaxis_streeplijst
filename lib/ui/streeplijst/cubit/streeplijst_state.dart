import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../core/entities/category/category.dart';
import '../../../core/entities/discount/discount.dart';
import '../../../core/entities/product/product.dart';

part 'streeplijst_state.freezed.dart';

@freezed
class StreeplijstState with _$StreeplijstState {
  const StreeplijstState._();

  const factory StreeplijstState({
    double? balance,
    required int categoryIndex,
    List<Category>? categories,
    List<Product>? products,
    List<Discount>? discounts,
  }) = _StreeplijstState;

  bool get isBalanceLoaded => balance != null;

  bool get isCategoriesLoaded => categories != null;

  bool get isProductsLoaded => products != null;

  bool get isDiscountsLoaded => discounts != null;

  bool get isInitialized =>
      isBalanceLoaded &&
      isCategoriesLoaded &&
      isProductsLoaded &&
      isDiscountsLoaded;
}
