// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'streeplijst_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$StreeplijstState {
  double? get balance => throw _privateConstructorUsedError;
  int get categoryIndex => throw _privateConstructorUsedError;
  List<Category>? get categories => throw _privateConstructorUsedError;
  List<Product>? get products => throw _privateConstructorUsedError;
  List<Discount>? get discounts => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $StreeplijstStateCopyWith<StreeplijstState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StreeplijstStateCopyWith<$Res> {
  factory $StreeplijstStateCopyWith(
          StreeplijstState value, $Res Function(StreeplijstState) then) =
      _$StreeplijstStateCopyWithImpl<$Res, StreeplijstState>;
  @useResult
  $Res call(
      {double? balance,
      int categoryIndex,
      List<Category>? categories,
      List<Product>? products,
      List<Discount>? discounts});
}

/// @nodoc
class _$StreeplijstStateCopyWithImpl<$Res, $Val extends StreeplijstState>
    implements $StreeplijstStateCopyWith<$Res> {
  _$StreeplijstStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? balance = freezed,
    Object? categoryIndex = null,
    Object? categories = freezed,
    Object? products = freezed,
    Object? discounts = freezed,
  }) {
    return _then(_value.copyWith(
      balance: freezed == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as double?,
      categoryIndex: null == categoryIndex
          ? _value.categoryIndex
          : categoryIndex // ignore: cast_nullable_to_non_nullable
              as int,
      categories: freezed == categories
          ? _value.categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<Category>?,
      products: freezed == products
          ? _value.products
          : products // ignore: cast_nullable_to_non_nullable
              as List<Product>?,
      discounts: freezed == discounts
          ? _value.discounts
          : discounts // ignore: cast_nullable_to_non_nullable
              as List<Discount>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_StreeplijstStateCopyWith<$Res>
    implements $StreeplijstStateCopyWith<$Res> {
  factory _$$_StreeplijstStateCopyWith(
          _$_StreeplijstState value, $Res Function(_$_StreeplijstState) then) =
      __$$_StreeplijstStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {double? balance,
      int categoryIndex,
      List<Category>? categories,
      List<Product>? products,
      List<Discount>? discounts});
}

/// @nodoc
class __$$_StreeplijstStateCopyWithImpl<$Res>
    extends _$StreeplijstStateCopyWithImpl<$Res, _$_StreeplijstState>
    implements _$$_StreeplijstStateCopyWith<$Res> {
  __$$_StreeplijstStateCopyWithImpl(
      _$_StreeplijstState _value, $Res Function(_$_StreeplijstState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? balance = freezed,
    Object? categoryIndex = null,
    Object? categories = freezed,
    Object? products = freezed,
    Object? discounts = freezed,
  }) {
    return _then(_$_StreeplijstState(
      balance: freezed == balance
          ? _value.balance
          : balance // ignore: cast_nullable_to_non_nullable
              as double?,
      categoryIndex: null == categoryIndex
          ? _value.categoryIndex
          : categoryIndex // ignore: cast_nullable_to_non_nullable
              as int,
      categories: freezed == categories
          ? _value._categories
          : categories // ignore: cast_nullable_to_non_nullable
              as List<Category>?,
      products: freezed == products
          ? _value._products
          : products // ignore: cast_nullable_to_non_nullable
              as List<Product>?,
      discounts: freezed == discounts
          ? _value._discounts
          : discounts // ignore: cast_nullable_to_non_nullable
              as List<Discount>?,
    ));
  }
}

/// @nodoc

class _$_StreeplijstState extends _StreeplijstState {
  const _$_StreeplijstState(
      {this.balance,
      required this.categoryIndex,
      final List<Category>? categories,
      final List<Product>? products,
      final List<Discount>? discounts})
      : _categories = categories,
        _products = products,
        _discounts = discounts,
        super._();

  @override
  final double? balance;
  @override
  final int categoryIndex;
  final List<Category>? _categories;
  @override
  List<Category>? get categories {
    final value = _categories;
    if (value == null) return null;
    if (_categories is EqualUnmodifiableListView) return _categories;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Product>? _products;
  @override
  List<Product>? get products {
    final value = _products;
    if (value == null) return null;
    if (_products is EqualUnmodifiableListView) return _products;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Discount>? _discounts;
  @override
  List<Discount>? get discounts {
    final value = _discounts;
    if (value == null) return null;
    if (_discounts is EqualUnmodifiableListView) return _discounts;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'StreeplijstState(balance: $balance, categoryIndex: $categoryIndex, categories: $categories, products: $products, discounts: $discounts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_StreeplijstState &&
            (identical(other.balance, balance) || other.balance == balance) &&
            (identical(other.categoryIndex, categoryIndex) ||
                other.categoryIndex == categoryIndex) &&
            const DeepCollectionEquality()
                .equals(other._categories, _categories) &&
            const DeepCollectionEquality().equals(other._products, _products) &&
            const DeepCollectionEquality()
                .equals(other._discounts, _discounts));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      balance,
      categoryIndex,
      const DeepCollectionEquality().hash(_categories),
      const DeepCollectionEquality().hash(_products),
      const DeepCollectionEquality().hash(_discounts));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_StreeplijstStateCopyWith<_$_StreeplijstState> get copyWith =>
      __$$_StreeplijstStateCopyWithImpl<_$_StreeplijstState>(this, _$identity);
}

abstract class _StreeplijstState extends StreeplijstState {
  const factory _StreeplijstState(
      {final double? balance,
      required final int categoryIndex,
      final List<Category>? categories,
      final List<Product>? products,
      final List<Discount>? discounts}) = _$_StreeplijstState;
  const _StreeplijstState._() : super._();

  @override
  double? get balance;
  @override
  int get categoryIndex;
  @override
  List<Category>? get categories;
  @override
  List<Product>? get products;
  @override
  List<Discount>? get discounts;
  @override
  @JsonKey(ignore: true)
  _$$_StreeplijstStateCopyWith<_$_StreeplijstState> get copyWith =>
      throw _privateConstructorUsedError;
}
