import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';

import '../../../core/use_cases/authentication/logout_use_case.dart';
import '../../../core/use_cases/balance/get_user_balance_use_case.dart';
import '../../../core/use_cases/categories/get_categories_use_case.dart';
import '../../../core/use_cases/discount/load_discounts_use_case.dart';
import '../../../core/use_cases/products/load_products_by_category_id_use_case.dart';
import 'streeplijst_state.dart';

export 'streeplijst_state.dart';

class StreeplijstCubit extends Cubit<StreeplijstState> {
  final LogoutUseCase _logoutUseCase;
  final GetUserBalanceUseCase _userBalanceUseCase;
  final GetCategoriesUseCase _getCategoriesUseCase;
  final LoadProductsByCategoryIdUseCase _loadProductsByCategoryIdUseCase;
  final LoadDiscountsUseCase _loadDiscountsUSeCase;

  StreeplijstCubit(
    this._logoutUseCase,
    this._userBalanceUseCase,
    this._getCategoriesUseCase,
    this._loadProductsByCategoryIdUseCase,
    this._loadDiscountsUSeCase,
  ) : super(StreeplijstState(categoryIndex: 0));

  Future<void> init() async {
    final categories = await _getCategoriesUseCase();
    emit(state.copyWith(categories: categories));
    selectCategory(state.categoryIndex);
    final balance = await _userBalanceUseCase();
    emit(state.copyWith(balance: balance));
    debugPrint('balance cubit = ${state.balance}');
    final discounts = await _loadDiscountsUSeCase();
    emit(state.copyWith(discounts: discounts));
  }

  Future<void> logout() {
    return _logoutUseCase();
  }

  Future<void> selectCategory(int index) async {
    emit(state.copyWith(categoryIndex: index));
    final products =
        await _loadProductsByCategoryIdUseCase(state.categories![index].id);
    emit(state.copyWith(products: products));
  }
}
