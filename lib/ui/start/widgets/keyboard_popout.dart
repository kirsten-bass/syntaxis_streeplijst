import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_listener/flutter_barcode_listener.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/login_cubit.dart';

class KeyboardPopout extends StatefulWidget {
  @override
  State<KeyboardPopout> createState() => KeyboardPopoutState();
}

class KeyboardPopoutState extends State<KeyboardPopout> {
  bool opened = false;

  final textEditingController = TextEditingController();

  void _submit() {
    final text = textEditingController.text;

    if (text.isNotEmpty) {
      context
          .read<LoginCubit>()
          .loginWithBarcode('be280999-39c2-4b42-98b2-f529cad5923d');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 200,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (opened) ...[
            DecoratedBox(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: TextField(
                controller: textEditingController,
                autofocus: true,
                style: TextStyle(
                  color: Colors.black,
                ),
                onSubmitted: (barcode) {
                  _submit();
                },
              ),
            ),
            SizedBox(height: 8.0),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Theme.of(context).colorScheme.secondary,
              ),
              onPressed: _submit,
              child: Text(
                'login'.tr(),
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      fontSize: 18,
                      height: 20 / 18,
                      color: Colors.black,
                    ),
              ),
            ),
          ],
          if (!opened)
            BarcodeKeyboardListener(
              onBarcodeScanned: (scanned) {
                if (scanned.isNotEmpty) {
                  context
                      .read<LoginCubit>()
                      .loginWithBarcode('be280999-39c2-4b42-98b2-f529cad5923d');
                }
              },
              bufferDuration: Duration(milliseconds: 200),
              child: Container(),
            ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: OutlinedButton(
              style: OutlinedButton.styleFrom(
                backgroundColor: Colors.black,
                side: BorderSide(
                  color: Theme.of(context).colorScheme.primary,
                ),
              ),
              onPressed: () {
                setState(() {
                  opened = !opened;
                });
                textEditingController.text = '';
                //FocusScope.of(context).requestFocus(focusNode);
              },
              child: Text(
                (opened ? 'close' : 'manual_barcode').tr(),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodySmall!.copyWith(
                      fontSize: 18,
                      height: 20 / 18,
                      color: Colors.white,
                    ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
