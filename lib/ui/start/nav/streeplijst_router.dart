import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../streeplijst/views/streeplijst_page.dart';
import '../views/landing_page.dart';
import '../views/splash_screen.dart';
import 'cubit/login_cubit.dart';

class StreeplijstRouter {
  StreeplijstRouter(this._mainNavigationCubit);

  final LoginCubit _mainNavigationCubit;

  RouteInformationParser<Object> get routeInformationParser =>
      router.routeInformationParser;

  RouterDelegate<Object> get routerDelegate => router.routerDelegate;

  late final router = GoRouter(
    routes: [
      GoRoute(
        path: SplashScreen.pagePath,
        pageBuilder: (context, state) =>
            MaterialPage(child: const SplashScreen()),
        redirect: (state) => LandingPage.pagePath,
      ),
      GoRoute(
        path: LandingPage.pagePath,
        pageBuilder: (context, state) =>
            MaterialPage(child: const LandingPage()),
      ),
      GoRoute(
        path: StreeplijstPage.pagePath,
        pageBuilder: (context, state) =>
            MaterialPage(child: const StreeplijstPage()),
      ),
    ],
    initialLocation: SplashScreen.pagePath,
    debugLogDiagnostics: kDebugMode,
    refreshListenable: GoRouterRefreshStream(_mainNavigationCubit.stream),
    redirect: (state) {
      return _mainNavigationCubit.state.when(
        loggedIn: () {
          if (state.subloc == LandingPage.pagePath) {
            return StreeplijstPage.pagePath;
          }

          return null;
        },
        loggedOut: () {
          if (state.subloc == StreeplijstPage.pagePath) {
            return LandingPage.pagePath;
          }

          return null;
        },
      );
    },
  );
}
