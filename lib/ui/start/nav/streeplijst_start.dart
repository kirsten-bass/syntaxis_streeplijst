import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/streeplijst_theme.dart';
import 'streeplijst_router.dart';

class StreeplijstStart extends StatelessWidget {
  StreeplijstStart({Key? key}) : super(key: key);

  late final StreeplijstRouter router;

  @override
  Widget build(BuildContext context) {
    final router = context.read<StreeplijstRouter>();

    return MaterialApp.router(
      scrollBehavior: TouchScrollBehaviour(),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'Syntaxis Streeplijst',
      theme: StreeplijstTheme.getTheme(context),
      debugShowCheckedModeBanner: kDebugMode,
      routerDelegate: router.routerDelegate,
      routeInformationParser: router.routeInformationParser,
    );
  }
}

class TouchScrollBehaviour extends MaterialScrollBehavior {
  // Override behavior methods and getters like dragDevices
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}
