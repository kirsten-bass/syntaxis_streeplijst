part of 'login_cubit.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class BarcodeListenerInitial extends LoginState {
  BarcodeListenerInitial();

  @override
  List<Object> get props => [];
}

class BarcodeListenerScanned extends LoginState {
  final String barcode;

  BarcodeListenerScanned(this.barcode);

  @override
  List<Object?> get props => [barcode];
}

class BarcodeListenerReset extends LoginState {
  @override
  List<Object?> get props => [];
}
