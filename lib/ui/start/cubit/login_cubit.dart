import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../core/use_cases/authentication/login_use_case.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final LoginUseCase _loginUseCase;

  LoginCubit(this._loginUseCase) : super(BarcodeListenerInitial());

  Future<void> loginWithBarcode(String barcode) async {
    ///TODO: change to actual barcode
    _loginUseCase.call(barcode);
  }
}
