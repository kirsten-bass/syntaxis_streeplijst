import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data_api/api_client.dart';
import '../../data_api/repositories/categories/api_category_repository.dart';
import '../../data_api/repositories/discounts/api_discounts_repository.dart';
import '../../data_api/repositories/login/api_login_repository.dart';
import '../../data_api/repositories/products/api_products_repository.dart';
import '../../data_api/repositories/transactions/api_transactions_repository.dart';
import '../../data_api/repositories/user/api_user_repository.dart';
import '../../data_mock/repositories/categories/mock_category_repository.dart';
import '../../data_mock/repositories/discounts/mock_discounts_repository.dart';
import '../../data_mock/repositories/login/mock_login_repository.dart';
import '../../data_mock/repositories/products/mock_product_repository.dart';
import '../../data_mock/repositories/transactions/mock_transaction_repository.dart';
import '../../data_mock/repositories/user/mock_user_repository.dart';
import '../repositories/categories/categories_repository.dart';
import '../repositories/discounts/discounts_repository.dart';
import '../repositories/products/products_repository.dart';
import '../repositories/transaction/transaction_repository.dart';
import '../repositories/user/user_repository.dart';
import 'authentication/authentication_provider.dart';

class ProviderBuilder {
  static List<RepositoryProvider> buildLoginMock() => [
        RepositoryProvider<AuthenticationProvider>(
            create: (_) => AuthenticationProvider(MockLoginRepository())),
      ];

  static List<RepositoryProvider> buildLogin() => [
        RepositoryProvider<Dio>(create: (_) => Dio()),
        RepositoryProvider<AuthenticationProvider>(
            create: (_) => AuthenticationProvider(ApiLoginRepository())),
      ];

  static List<RepositoryProvider> buildApp(BuildContext context) {
    final apiClient = ApiClient(
      Dio(
        BaseOptions(
          headers: {
            'authorization':
                'Bearer ${context.read<AuthenticationProvider>().accessToken!}'
          },
        ),
      ),
    );

    return [
      RepositoryProvider<CategoriesRepository>(
          create: (_) => ApiCategoryRepository(apiClient)),
      RepositoryProvider<DiscountsRepository>(
          create: (_) => ApiDiscountsRepository(apiClient)),
      RepositoryProvider<ProductsRepository>(
          create: (_) => ApiProductsRepository(apiClient)),
      RepositoryProvider<TransactionsRepository>(
          create: (_) => ApiTransactionsRepository(apiClient)),
      RepositoryProvider<UserRepository>(
          create: (_) => ApiUserRepository(apiClient))
    ];
  }

  static List<RepositoryProvider> buildAppMock() => [
        RepositoryProvider<CategoriesRepository>(
            create: (_) => MockCategoryRepository()),
        RepositoryProvider<DiscountsRepository>(
            create: (_) => MockDiscountsRepository()),
        RepositoryProvider<ProductsRepository>(
            create: (_) => MockProductsRepository()),
        RepositoryProvider<TransactionsRepository>(
            create: (_) => MockTransactionsRepository()),
        RepositoryProvider<UserRepository>(create: (_) => MockUserRepository())
      ];
}
