import 'package:freezed_annotation/freezed_annotation.dart';

import '../transaction_item/transaction_item.dart';
import '../user/user.dart';

part 'transaction.freezed.dart';

part 'transaction.g.dart';

@freezed
class TransactionWrite with _$TransactionWrite {
  const factory TransactionWrite({
    required List<TransactionItemWrite> transactionItems,
  }) = _TransactionWrite;

  factory TransactionWrite.fromJson(Map<String, dynamic> json) =>
      _$TransactionWriteFromJson(json);
}

@freezed
class TransactionRead with _$TransactionRead {
  const factory TransactionRead({
    required int? id,
    required double? balanceChange,
    required DateTime createdOn,
    required List<TransactionItemRead> transactionItems,
  }) = _TransactionRead;

  factory TransactionRead.fromJson(Map<String, dynamic> json) =>
      _$TransactionReadFromJson(json);
}