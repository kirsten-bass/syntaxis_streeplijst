// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TransactionWrite _$$_TransactionWriteFromJson(Map<String, dynamic> json) =>
    _$_TransactionWrite(
      transactionItems: (json['transactionItems'] as List<dynamic>)
          .map((e) => TransactionItemWrite.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TransactionWriteToJson(_$_TransactionWrite instance) =>
    <String, dynamic>{
      'transactionItems': instance.transactionItems,
    };

_$_TransactionRead _$$_TransactionReadFromJson(Map<String, dynamic> json) =>
    _$_TransactionRead(
      id: json['id'] as int?,
      balanceChange: (json['balanceChange'] as num?)?.toDouble(),
      createdOn: DateTime.parse(json['createdOn'] as String),
      transactionItems: (json['transactionItems'] as List<dynamic>)
          .map((e) => TransactionItemRead.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_TransactionReadToJson(_$_TransactionRead instance) =>
    <String, dynamic>{
      'id': instance.id,
      'balanceChange': instance.balanceChange,
      'createdOn': instance.createdOn.toIso8601String(),
      'transactionItems': instance.transactionItems,
    };
