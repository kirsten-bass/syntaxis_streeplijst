import 'package:freezed_annotation/freezed_annotation.dart';

part 'discount.freezed.dart';

part 'discount.g.dart';

@freezed
class Discount with _$Discount {
  const factory Discount({
    required int id,
    required String name,
    required double percentage,
  }) = _Discount;

  factory Discount.fromJson(Map<String, dynamic> json) =>
      _$DiscountFromJson(json);
}
