import 'package:freezed_annotation/freezed_annotation.dart';

import '../image/image.dart';

part 'product.freezed.dart';

part 'product.g.dart';

@freezed
class Product with _$Product {
  const Product._();

  const factory Product({
    required int id,
    required double price,
    required double discountPct,
    required String name,
    required double deposit,
    Image? image,
  }) = _Product;

  bool get hasDiscount => discountPct > 0;

  bool get hasDeposit => deposit > 0;

  double get totalPrice => price * (1 - discountPct) + deposit;

  double get discountAmount => price - (price * (1 - discountPct));

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
}

enum DiscountType { NONE, SINGLE }
