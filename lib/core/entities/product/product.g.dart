// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Product _$$_ProductFromJson(Map<String, dynamic> json) => _$_Product(
      id: json['id'] as int,
      price: (json['price'] as num).toDouble(),
      discountPct: (json['discountPct'] as num).toDouble(),
      name: json['name'] as String,
      deposit: (json['deposit'] as num).toDouble(),
      image: json['image'] == null
          ? null
          : Image.fromJson(json['image'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ProductToJson(_$_Product instance) =>
    <String, dynamic>{
      'id': instance.id,
      'price': instance.price,
      'discountPct': instance.discountPct,
      'name': instance.name,
      'deposit': instance.deposit,
      'image': instance.image,
    };
