import 'package:freezed_annotation/freezed_annotation.dart';

part 'pageable.freezed.dart';

part 'pageable.g.dart';

@freezed
class Pageable with _$Pageable {
  const factory Pageable({
    required bool last,
    required int totalPages,
    required int totalElements,
    required int size,
    required int number,
    required bool first,
    required bool empty,
    required int numberOfElements,
    required List<Map<String, dynamic>> content,
  }) = _Pageable;

  factory Pageable.fromJson(Map<String, dynamic> json) =>
      _$PageableFromJson(json);
}