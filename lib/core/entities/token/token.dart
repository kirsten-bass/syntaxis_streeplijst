import 'package:freezed_annotation/freezed_annotation.dart';

import '../user/user.dart';

part 'token.freezed.dart';

part 'token.g.dart';

@freezed
class Token with _$Token {

  factory Token({
    required String accessToken,
    required String refreshToken,
    required User user,
  }) = _Token;

  factory Token.fromJson(Map<String, dynamic> json) => _$TokenFromJson(json);
}

@freezed
class LoginRequest with _$LoginRequest {

  factory LoginRequest({
    required String token,
  }) = _LoginRequest;

  factory LoginRequest.fromJson(Map<String, dynamic> json) => _$LoginRequestFromJson(json);
}