// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TransactionItemWrite _$$_TransactionItemWriteFromJson(
        Map<String, dynamic> json) =>
    _$_TransactionItemWrite(
      amount: json['amount'] as int,
      product: json['product'] as int,
    );

Map<String, dynamic> _$$_TransactionItemWriteToJson(
        _$_TransactionItemWrite instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'product': instance.product,
    };

_$_TransactionItemRead _$$_TransactionItemReadFromJson(
        Map<String, dynamic> json) =>
    _$_TransactionItemRead(
      balanceChange: (json['balanceChange'] as num).toDouble(),
      refunded: json['refunded'] as bool,
      product: Product.fromJson(json['product'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_TransactionItemReadToJson(
        _$_TransactionItemRead instance) =>
    <String, dynamic>{
      'balanceChange': instance.balanceChange,
      'refunded': instance.refunded,
      'product': instance.product,
    };
