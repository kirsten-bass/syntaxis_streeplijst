// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'transaction_item.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TransactionItemWrite _$TransactionItemWriteFromJson(Map<String, dynamic> json) {
  return _TransactionItemWrite.fromJson(json);
}

/// @nodoc
mixin _$TransactionItemWrite {
  int get amount => throw _privateConstructorUsedError;
  int get product => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionItemWriteCopyWith<TransactionItemWrite> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionItemWriteCopyWith<$Res> {
  factory $TransactionItemWriteCopyWith(TransactionItemWrite value,
          $Res Function(TransactionItemWrite) then) =
      _$TransactionItemWriteCopyWithImpl<$Res, TransactionItemWrite>;
  @useResult
  $Res call({int amount, int product});
}

/// @nodoc
class _$TransactionItemWriteCopyWithImpl<$Res,
        $Val extends TransactionItemWrite>
    implements $TransactionItemWriteCopyWith<$Res> {
  _$TransactionItemWriteCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? product = null,
  }) {
    return _then(_value.copyWith(
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TransactionItemWriteCopyWith<$Res>
    implements $TransactionItemWriteCopyWith<$Res> {
  factory _$$_TransactionItemWriteCopyWith(_$_TransactionItemWrite value,
          $Res Function(_$_TransactionItemWrite) then) =
      __$$_TransactionItemWriteCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int amount, int product});
}

/// @nodoc
class __$$_TransactionItemWriteCopyWithImpl<$Res>
    extends _$TransactionItemWriteCopyWithImpl<$Res, _$_TransactionItemWrite>
    implements _$$_TransactionItemWriteCopyWith<$Res> {
  __$$_TransactionItemWriteCopyWithImpl(_$_TransactionItemWrite _value,
      $Res Function(_$_TransactionItemWrite) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? amount = null,
    Object? product = null,
  }) {
    return _then(_$_TransactionItemWrite(
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as int,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TransactionItemWrite implements _TransactionItemWrite {
  const _$_TransactionItemWrite({required this.amount, required this.product});

  factory _$_TransactionItemWrite.fromJson(Map<String, dynamic> json) =>
      _$$_TransactionItemWriteFromJson(json);

  @override
  final int amount;
  @override
  final int product;

  @override
  String toString() {
    return 'TransactionItemWrite(amount: $amount, product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TransactionItemWrite &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.product, product) || other.product == product));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, amount, product);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TransactionItemWriteCopyWith<_$_TransactionItemWrite> get copyWith =>
      __$$_TransactionItemWriteCopyWithImpl<_$_TransactionItemWrite>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TransactionItemWriteToJson(
      this,
    );
  }
}

abstract class _TransactionItemWrite implements TransactionItemWrite {
  const factory _TransactionItemWrite(
      {required final int amount,
      required final int product}) = _$_TransactionItemWrite;

  factory _TransactionItemWrite.fromJson(Map<String, dynamic> json) =
      _$_TransactionItemWrite.fromJson;

  @override
  int get amount;
  @override
  int get product;
  @override
  @JsonKey(ignore: true)
  _$$_TransactionItemWriteCopyWith<_$_TransactionItemWrite> get copyWith =>
      throw _privateConstructorUsedError;
}

TransactionItemRead _$TransactionItemReadFromJson(Map<String, dynamic> json) {
  return _TransactionItemRead.fromJson(json);
}

/// @nodoc
mixin _$TransactionItemRead {
  double get balanceChange => throw _privateConstructorUsedError;
  bool get refunded => throw _privateConstructorUsedError;
  Product get product => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TransactionItemReadCopyWith<TransactionItemRead> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TransactionItemReadCopyWith<$Res> {
  factory $TransactionItemReadCopyWith(
          TransactionItemRead value, $Res Function(TransactionItemRead) then) =
      _$TransactionItemReadCopyWithImpl<$Res, TransactionItemRead>;
  @useResult
  $Res call({double balanceChange, bool refunded, Product product});

  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class _$TransactionItemReadCopyWithImpl<$Res, $Val extends TransactionItemRead>
    implements $TransactionItemReadCopyWith<$Res> {
  _$TransactionItemReadCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? balanceChange = null,
    Object? refunded = null,
    Object? product = null,
  }) {
    return _then(_value.copyWith(
      balanceChange: null == balanceChange
          ? _value.balanceChange
          : balanceChange // ignore: cast_nullable_to_non_nullable
              as double,
      refunded: null == refunded
          ? _value.refunded
          : refunded // ignore: cast_nullable_to_non_nullable
              as bool,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ProductCopyWith<$Res> get product {
    return $ProductCopyWith<$Res>(_value.product, (value) {
      return _then(_value.copyWith(product: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_TransactionItemReadCopyWith<$Res>
    implements $TransactionItemReadCopyWith<$Res> {
  factory _$$_TransactionItemReadCopyWith(_$_TransactionItemRead value,
          $Res Function(_$_TransactionItemRead) then) =
      __$$_TransactionItemReadCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double balanceChange, bool refunded, Product product});

  @override
  $ProductCopyWith<$Res> get product;
}

/// @nodoc
class __$$_TransactionItemReadCopyWithImpl<$Res>
    extends _$TransactionItemReadCopyWithImpl<$Res, _$_TransactionItemRead>
    implements _$$_TransactionItemReadCopyWith<$Res> {
  __$$_TransactionItemReadCopyWithImpl(_$_TransactionItemRead _value,
      $Res Function(_$_TransactionItemRead) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? balanceChange = null,
    Object? refunded = null,
    Object? product = null,
  }) {
    return _then(_$_TransactionItemRead(
      balanceChange: null == balanceChange
          ? _value.balanceChange
          : balanceChange // ignore: cast_nullable_to_non_nullable
              as double,
      refunded: null == refunded
          ? _value.refunded
          : refunded // ignore: cast_nullable_to_non_nullable
              as bool,
      product: null == product
          ? _value.product
          : product // ignore: cast_nullable_to_non_nullable
              as Product,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TransactionItemRead implements _TransactionItemRead {
  const _$_TransactionItemRead(
      {required this.balanceChange,
      required this.refunded,
      required this.product});

  factory _$_TransactionItemRead.fromJson(Map<String, dynamic> json) =>
      _$$_TransactionItemReadFromJson(json);

  @override
  final double balanceChange;
  @override
  final bool refunded;
  @override
  final Product product;

  @override
  String toString() {
    return 'TransactionItemRead(balanceChange: $balanceChange, refunded: $refunded, product: $product)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TransactionItemRead &&
            (identical(other.balanceChange, balanceChange) ||
                other.balanceChange == balanceChange) &&
            (identical(other.refunded, refunded) ||
                other.refunded == refunded) &&
            (identical(other.product, product) || other.product == product));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, balanceChange, refunded, product);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TransactionItemReadCopyWith<_$_TransactionItemRead> get copyWith =>
      __$$_TransactionItemReadCopyWithImpl<_$_TransactionItemRead>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TransactionItemReadToJson(
      this,
    );
  }
}

abstract class _TransactionItemRead implements TransactionItemRead {
  const factory _TransactionItemRead(
      {required final double balanceChange,
      required final bool refunded,
      required final Product product}) = _$_TransactionItemRead;

  factory _TransactionItemRead.fromJson(Map<String, dynamic> json) =
      _$_TransactionItemRead.fromJson;

  @override
  double get balanceChange;
  @override
  bool get refunded;
  @override
  Product get product;
  @override
  @JsonKey(ignore: true)
  _$$_TransactionItemReadCopyWith<_$_TransactionItemRead> get copyWith =>
      throw _privateConstructorUsedError;
}
