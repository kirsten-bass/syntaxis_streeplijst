import 'package:freezed_annotation/freezed_annotation.dart';

import '../discount/discount.dart';
import '../product/product.dart';

part 'transaction_item.freezed.dart';

part 'transaction_item.g.dart';

@freezed
class TransactionItemWrite with _$TransactionItemWrite {
  const factory TransactionItemWrite({
    required int amount,
    required int product,
  }) = _TransactionItemWrite;

  factory TransactionItemWrite.fromJson(Map<String, dynamic> json) =>
      _$TransactionItemWriteFromJson(json);
}

@freezed
class TransactionItemRead with _$TransactionItemRead {
  const factory TransactionItemRead({
    required double balanceChange,
    required bool refunded,
    required Product product,
  }) = _TransactionItemRead;

  factory TransactionItemRead.fromJson(Map<String, dynamic> json) =>
      _$TransactionItemReadFromJson(json);
}
