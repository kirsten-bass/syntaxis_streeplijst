import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

part 'user.g.dart';

@freezed
class User with _$User {
  const User._();

  const factory User({
    //required String? email,
    //required String? barcode,
    required String? firstname,
    required String? lastname,
    required int id,
    //required double balance,
    //required String? roles,
    //required bool discount,
  }) = _User;

  get fullname => '$firstname $lastname';

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
