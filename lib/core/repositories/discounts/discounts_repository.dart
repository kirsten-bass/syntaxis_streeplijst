import '../../entities/discount/discount.dart';

mixin DiscountsRepository {
  Future<List<Discount>> getDiscounts();
}
