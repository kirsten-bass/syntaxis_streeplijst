import '../../entities/transaction/transaction.dart';

mixin TransactionsRepository {
  Future<void> addTransaction(TransactionWrite transaction);
}
