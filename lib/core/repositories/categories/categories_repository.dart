import '../../entities/category/category.dart';

mixin CategoriesRepository {
  Future<List<Category>> getCategories();
}
