import 'dart:convert';

import 'package:http/http.dart' as http;

class BingDailyImageGetterUseCase {
  Future<String> call() async {
    var url = Uri.parse(
        'https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=nl-NL');
    var response = await http.get(url, headers: {'Accept': 'application/json'});
    var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;
    var imagePath = decodedResponse['images'][0]['url'];
    return 'https://bing.com$imagePath';
  }
}
