import '../../entities/discount/discount.dart';
import '../../repositories/discounts/discounts_repository.dart';

class LoadDiscountsUseCase {
  final DiscountsRepository _repository;

  LoadDiscountsUseCase(this._repository);

  Future<List<Discount>> call() {
    return _repository.getDiscounts();
  }
}
