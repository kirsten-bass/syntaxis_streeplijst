import 'package:flutter/material.dart';

/// Class that helps build a MaterialStateValue, returns de [defaultValue]
/// unless a different value is set for the specific state.
class MaterialStateValueBuilder<T> extends MaterialStateProperty<T> {
  /// Value that is returned when the [MaterialState] value has no
  /// defined property.
  T defaultValue;

  /// Value that is returned when the [MaterialState]'s value is
  /// [MaterialState.pressed].
  T? pressedValue;

  /// Value that is returned when the [MaterialState]'s value is
  /// [MaterialState.selected].
  T? selectedValue;

  /// Value that is returned when the [MaterialState]'s value is
  /// [MaterialState.disabled].
  T? disabledValue;

  /// Value that is returned when the [MaterialState]'s value is
  /// [MaterialState.dragged].
  T? draggedValue;

  /// Value that is returned when the [MaterialState]'s value is
  /// [MaterialState.focused].
  T? focusedValue;

  /// Value that is returned when the [MaterialState]'s value is
  /// [MaterialState.error].
  T? errorValue;

  /// Value that is returned when the [MaterialState]'s value is
  /// [MaterialState.hovered].
  T? hoveredValue;

  /// Constructs a MaterialStateValue. Defaults to [defaultValue] if no [Value]
  /// has been set for certain [MaterialState]s.
  MaterialStateValueBuilder({
    required this.defaultValue,
    this.pressedValue,
    this.selectedValue,
    this.disabledValue,
    this.draggedValue,
    this.focusedValue,
    this.errorValue,
    this.hoveredValue,
  });

  /// This method returns a value based on the possible
  /// state of the elevated button.
  /// More states could be added if needed.
  @override
  T resolve(Set<MaterialState> states) {
    if (states.any((x) => x == MaterialState.pressed)) {
      return pressedValue ?? defaultValue;
    } else if (states.any((x) => x == MaterialState.selected)) {
      return selectedValue ?? defaultValue;
    } else if (states.any((x) => x == MaterialState.disabled)) {
      return disabledValue ?? defaultValue;
    } else if (states.any((x) => x == MaterialState.dragged)) {
      return draggedValue ?? defaultValue;
    } else if (states.any((x) => x == MaterialState.focused)) {
      return focusedValue ?? defaultValue;
    } else if (states.any((x) => x == MaterialState.error)) {
      return errorValue ?? defaultValue;
    } else if (states.any((x) => x == MaterialState.hovered)) {
      return hoveredValue ?? defaultValue;
    } else {
      return defaultValue;
    }
  }
}
